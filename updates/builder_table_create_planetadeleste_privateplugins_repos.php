<?php namespace PlanetaDelEste\PrivatePlugins\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePlanetadelestePrivatepluginsRepos extends Migration
{
    public function up()
    {
        Schema::create('planetadeleste_privateplugins_repos', function($table)
        {
            /** @var \October\Rain\Database\Schema\Blueprint $table */
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('last_commit')->nullable();
            $table->string('source', 16)->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('uuid')->nullable();
            $table->string('code')->nullable();
            $table->string('commit')->nullable();
            $table->boolean('has_update')->default(false);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('planetadeleste_privateplugins_repos');
    }
}
