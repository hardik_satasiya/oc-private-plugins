+function ($) {
    "use strict";

    var repoProcess = function () {
        this.init();
    };

    repoProcess.prototype.init = function () {
        this.activeStep = null;
        this.steps      = null
    };

    repoProcess.prototype.form = function () {
        return $('#repoForm');
    };

    repoProcess.prototype.check = function () {
        this.form().request('onLoadRepo');
    };

    repoProcess.prototype.install = function (steps) {
        this.steps = steps;
        this.runChain();

        // this.$form.request('onDownloadRepo');
    };

    repoProcess.prototype.retryUpdate = function() {
        $('#executeActivity').show();
        $('#executeStatus').html('');

        this.runChain(this.activeStep)
    };

    repoProcess.prototype.runChain = function (fromStep) {
        $.waterfall.apply(this, this.buildEventChain(this.steps, fromStep))
         .fail(function (reason) {
             var
                 template = $('#executeFailed').html(),
                 html     = Mustache.to_html(template, { reason: reason });

             $('#executeActivity').hide();
             $('#executeStatus').html(html)
         })
    };

    repoProcess.prototype.buildEventChain = function (steps, fromStep) {
        var self       = this,
            eventChain = [],
            skipStep   = !!fromStep;

        console.log(steps, skipStep);

        $.each(steps, function (index, step) {

            if (step === fromStep) {
                skipStep = false
            }

            if (skipStep) {
                return true // Continue
            }

            eventChain.push(function () {
                var deferred = $.Deferred();

                self.activeStep = step;
                self.setLoadingBar(true, step.label);

                self.form().request('onExecuteStep', {
                    data   : step,
                    success: function (data) {
                        setTimeout(function () { deferred.resolve() }, 600);

                        this.success(data);

                        if (step.code !== 'complete')
                            self.setLoadingBar(false)
                    },
                    error  : function (data) {
                        self.setLoadingBar(false);
                        deferred.reject(data.responseText)
                    }
                });

                return deferred
            })
        });

        return eventChain
    };

    repoProcess.prototype.setLoadingBar = function (state, message) {
        var loadingBar = $('#executeLoadingBar'),
            messageDiv = $('#executeMessage');

        if (state)
            loadingBar.removeClass('bar-loaded');
        else
            loadingBar.addClass('bar-loaded');

        if (message)
            messageDiv.text(message)
    };

    if ($.privatePlugins === undefined)
        $.privatePlugins = {};

    $.privatePlugins.repoProcess = new repoProcess;

}(window.jQuery);