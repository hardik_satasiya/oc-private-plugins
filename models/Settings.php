<?php namespace PlanetaDelEste\PrivatePlugins\Models;

use Model;

/**
 * Settings Model
 *
 * @method static null|mixed get(string $key, $default = null)
 * @implement \System\Behaviors\SettingsModel
 */
class Settings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'planetadeleste_privateplugins_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

    public function initSettingsData()
    {
        if (!$this->id) {
            $this->save();
        }
    }

}
