<?php namespace PlanetaDelEste\PrivatePlugins\Models;

use Model;
use System\Classes\PluginManager;

/**
 * Model
 *
 * @property \October\Rain\Argon\Argon created_at
 * @property \October\Rain\Argon\Argon updated_at
 * @property \October\Rain\Argon\Argon last_commit
 * @property string                    source
 * @property string                    name
 * @property string                    slug
 * @property string                    uuid
 * @property string                    code
 * @property string                    commit
 * @property boolean                   has_update
 */
class Repo extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'planetadeleste_privateplugins_repos';

    protected $fillable = [
        'source',
        'name',
        'slug',
        'uuid',
        'code',
        'commit'
    ];


    public function beforeDelete()
    {
        $pluginManager = PluginManager::instance();
        if($pluginManager->findByIdentifier($this->code)) {
            $pluginManager->deletePlugin($this->code);
        }
    }
}
