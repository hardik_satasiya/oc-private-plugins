<?php namespace PlanetaDelEste\PrivatePlugins;

use Illuminate\Foundation\AliasLoader;
use PlanetaDelEste\PrivatePlugins\Controllers\Repos;
use PlanetaDelEste\PrivatePlugins\Models\Settings;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
        return [
            'planetadeleste_privateplugins_settings' => [
                'label' => 'planetadeleste.privateplugins::lang.settings.menu_label',
                'description' => 'planetadeleste.privateplugins::lang.settings.menu_description',
                'category' => 'planetadeleste.privateplugins::lang.settings.category',
                'icon' => 'icon-cog',
                'class' => '\PlanetaDelEste\PrivatePlugins\Models\Settings',
                'order' => 500,
                'keywords' => 'settings private plugins'
            ]
        ];
    }

    public function boot()
    {
        $this->setRepositriesConfig();

        Repos::extend(function ($controller) {
            /** @var \Backend\Classes\Controller $controller */
            if (!$controller->isClassExtendedWith('PlanetaDelEste.PrivatePlugins.Behaviors.BitbucketController') && config('bitbucket.enabled')) {
                $controller->implement[] = 'PlanetaDelEste.PrivatePlugins.Behaviors.BitbucketController';
            }

            if (!$controller->isClassExtendedWith('PlanetaDelEste.PrivatePlugins.Behaviors.GithubController') && config('github.enabled')) {
                $controller->implement[] = 'PlanetaDelEste.PrivatePlugins.Behaviors.GithubController';
            }
        });

        \App::register(\GrahamCampbell\Bitbucket\BitbucketServiceProvider::class);
        \App::register(\GrahamCampbell\GitHub\GitHubServiceProvider::class);

        $alias = AliasLoader::getInstance();
        $alias->alias('Bitbucket', \GrahamCampbell\Bitbucket\Facades\Bitbucket::class);
        $alias->alias('GitHub', \GrahamCampbell\GitHub\Facades\GitHub::class);
    }

    protected function setRepositriesConfig()
    {
        /*
         * Set Bitbucket config
         */
        $bitbucketMethod = Settings::get('bitbucket.method');
        $bitbucketEnabled = false;
        if ($bitbucketMethod) {
            $bitbucketConfig = array_filter(Settings::get('bitbucket.'.$bitbucketMethod));
            if ($bitbucketConfig) {
                $bitbucketConfig['method'] = $bitbucketMethod;
//                $bitbucketConfig['version'] = '2.0';
                $bitbucketConnections = array_merge(
                    config('bitbucket.connections', []),
                    [$bitbucketMethod => $bitbucketConfig]
                );

                \Config::set('bitbucket.connections', $bitbucketConnections);
                \Config::set('bitbucket.default', $bitbucketMethod);
                \Config::set('bitbucket.account', Settings::get('bitbucket.account'));
                $bitbucketEnabled = true;
            }
        }
        \Config::set('bitbucket.enabled', $bitbucketEnabled);

        /*
         * Set GitHub config
         */
        $githubMethod = Settings::get('github.method');
        $githubEnabled = false;
        if ($githubMethod) {
            $githubConfig = array_filter(Settings::get('github.'.$githubMethod));
            if ($githubConfig) {
                $githubConfig['method'] = $githubMethod;
                $githubConnections = array_merge(config('github.connections', []), [$githubMethod => $githubConfig]);

                \Config::set('github.connections', $githubConnections);
                \Config::set('github.default', $githubMethod);
                \Config::set('github.account', Settings::get('github.account'));
                $githubEnabled = true;
            }
        }
        \Config::set('github.enabled', $githubEnabled);
    }
}
