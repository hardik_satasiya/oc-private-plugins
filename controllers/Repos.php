<?php namespace PlanetaDelEste\PrivatePlugins\Controllers;

use ApplicationException;
use BackendMenu;
use Backend\Classes\Controller;
use Carbon\Carbon;
use Flash;
use Lang;
use October\Rain\Filesystem\Zip;
use PlanetaDelEste\PrivatePlugins\Classes\ComposerManager;
use PlanetaDelEste\PrivatePlugins\Models\Repo;
use System\Classes\PluginManager;
use System\Classes\UpdateManager;

/**
 * Repos Back-end Controller
 *
 * @implement \Backend\Behaviors\FormController
 */
class Repos extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $account = null;
    public $source = null;
    public $slug = null;

    public function __construct()
    {
        parent::__construct();

        $this->addJs('/plugins/planetadeleste/privateplugins/assets/js/repo.js');
        $this->addCss('/plugins/planetadeleste/privateplugins/assets/css/repos.css');
        $this->addCss('/modules/system/assets/css/updates/updates.css');

        $this->source = $this->vars['source'] = post('source');
        $this->slug = $this->vars['slug'] = post('slug');
        $this->account = config($this->source.'.account');

        $this->vars['node'] = post('node');
        $this->vars['namespace'] = post('namespace');
        $this->vars['file'] = post('file');
        $this->vars['has_composer'] = post('has_composer');

        BackendMenu::setContext('PlanetaDelEste.PrivatePlugins', 'privateplugins', 'repos');
    }

    /**
     * @param null $context
     *
     * @return mixed
     * @throws \Exception
     */
    public function create($context = null)
    {
        return $this->asExtension('FormController')->create($context);
    }

    protected function buildSteps()
    {
        $steps = [
            [
                'code'  => 'download',
                'label' => trans(
                    'planetadeleste.privateplugins::lang.repo.downloading',
                    ['name' => $this->slug]
                )
            ],
            [
                'code'  => 'extract',
                'label' => trans(
                    'planetadeleste.privateplugins::lang.repo.extracting',
                    ['name' => $this->slug]
                )
            ],
            [
                'code'  => 'validate',
                'label' => trans('planetadeleste.privateplugins::lang.repo.validating_plugin')
            ],
        ];

        if ($hasComposer = array_get($this->vars, 'has_composer')) {
            $steps[] = [
                'code'  => 'composer',
                'label' => trans('planetadeleste.privateplugins::lang.repo.composer_installing'),
            ];
        }

        $steps[] = [
            'code'  => 'complete',
            'label' => trans('planetadeleste.privateplugins::lang.repo.install_completing'),
        ];

        return $steps;
    }

    /**
     * @return mixed
     * @throws \SystemException
     */
    protected function getFormVars()
    {
        return $this->makePartial('form_vars_php');
    }

    /**
     * @throws \ApplicationException
     */
    protected function extract()
    {
        $file = post('file');
        list($author, $plugin) = explode('.', strtolower(post('namespace')));

        $destination = plugins_path($author.'/');
        if (!\File::exists($destination)) {
            \File::makeDirectory($destination, 775, true);
        }

        $zip = new Zip();

        if (!$zip->open($file)) {
            throw new ApplicationException(trans('system::lang.zip.extract_failed', ['file' => $file]));
        }

        $folder = trim($zip->getNameIndex(0), '/');
        $zip->extractTo($destination);

        if(\File::exists($destination.$plugin)) {
            \File::deleteDirectory($destination.$plugin);
        }

        \File::moveDirectory($destination.$folder, $destination.$plugin);
        \File::delete($file);

        $repo = $this->getRepo();
        $model = Repo::where('slug', array_get($repo, 'full_name'))
            ->where('source', $this->source)
            ->first();

        if(!$model) {
            $model = new Repo(
                [
                    'source' => $this->source,
                    'name'   => array_get($repo, 'name'),
                    'slug'   => array_get($repo, 'full_name'),
                    'uuid'   => array_has($repo, 'uuid') ? array_get($repo, 'uuid') : array_get($repo, 'id'),
                    'code'   => post('namespace'),
                ]
            );
        }

        $model->commit = post('node');
        $model->last_commit = Carbon::parse( array_get($repo, 'updated_on') );
        $model->save();
    }

    /**
     * @param $plugin
     *
     * @return array
     */
    public function findDependences($plugin)
    {
        $pluginManager = PluginManager::instance();
        $required = $pluginManager->getDependencies($plugin);

        return $this->checkDependences($required);

    }

    /**
     * @param null|array $required
     *
     * @return array
     */
    public function checkDependences($required)
    {
        $pluginManager = PluginManager::instance();
        $depends = [];
        if (count($required)) {
            foreach ($required as $require) {
                $disable = false;
                if (!$pluginObj = $pluginManager->findByIdentifier($require)) {
                    $disable = true;
                } elseif ($pluginObj->disabled) {
                    $disable = true;
                }
                $depends[] = [$require, $disable];
            }
        }

        return $depends;
    }

    /**
     * @param array $content
     *
     * @return string
     */
    public function getRawNamespace($content)
    {
        $namespace = array_filter(
            $content,
            function ($val) {
                return stripos($val, 'namespace') !== false;
            }
        );
        $namespace = array_shift($namespace);
        return trim(substr($namespace, stripos($namespace, 'namespace') + 9, -1));
    }

    /**
     * @param array $content
     *
     * @return array
     */
    public function getRawDependencies($content)
    {
        $require = [];
        $required = array_filter(
            $content,
            function ($val) {
                return stripos($val, 'public $require') !== false;
            }
        );
        $required = array_shift($required);
        eval(trim(str_replace('public', 'return', $required)));

        return $this->checkDependences($require);
    }

    public function getRepo()
    {
        if($this->account && $this->slug && $this->source) {
            $method = camel_case("get_{$this->source}_repo");
            return call_user_func([$this, $method]);
        }

        return null;
    }

    /**
     * Deleted checked repos.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $repoId) {
                if (!$repo = Repo::find($repoId)) {
                    continue;
                }
                $repo->delete();
            }

            Flash::success(Lang::get('planetadeleste.privateplugins::lang.repos.delete_selected_success'));
        } else {
            Flash::error(Lang::get('planetadeleste.privateplugins::lang.repos.delete_selected_empty'));
        }

        return $this->listRefresh();
    }

    /**
     * Load all repositories from account
     *
     * @return array
     */
    public function onLoadRepos()
    {
        try {
            if (!config($this->source.'.enabled')) {
                return [
                    '#repo-list' => $this->makeHintPartial(
                        'hint_warn_php',
                        ['type' => 'warning', 'icon' => 'icon-'.$this->source, 'repo' => studly_case($this->source)]
                    )
                ];
            }

            $method = camel_case("get_{$this->source}_repos");
            $this->vars['list'] = call_user_func([$this, $method]);

            return ['#repo-list' => $this->makePartial('repo_list_php')];
        } catch (\SystemException $e) {
            trace_log($e);
            Flash::error($e->getMessage());
        }
    }

    /**
     * Init popup html
     *
     * @return array
     * @throws \SystemException
     */
    public function onLoadRepoModal()
    {
        $data = post();

        try {
            $this->vars['path'] = array_get($data, 'path');
            return ['result' => $this->makePartial('repo_form_php')];
        } catch (\SystemException $e) {
            $this->handleError($e);
            return $this->makePartial('repo_form_php');
        }
    }

    /**
     *
     */
    public function onLoadRepo()
    {
        try {
            $method = camel_case("load_{$this->source}_repo");
            $content = call_user_func([$this, $method]);

            return ['#repoPopupContainer' => $content, '#formVars' => $this->getFormVars()];

        } catch (\SystemException $e) {
            trace_log($e);
            Flash::error($e->getMessage());
        }
    }

    public function onInstallPlugin()
    {
        try {
            $this->vars['steps'] = $this->buildSteps();

            return [
                '#repoPopupContainer' => $this->makePartial('install_popup_php'),
                '#formVars'           => $this->getFormVars()
            ];
        } catch (\SystemException $e) {
            trace_log($e);
            Flash::error($e->getMessage());
        }
    }

    /**
     * @return array|mixed
     * @throws \SystemException
     * @throws \Exception
     */
    public function onExecuteStep()
    {
        $stepCode = post('code');

        switch ($stepCode) {

            case 'download':
                $method = camel_case("download_{$this->source}_repo");
                call_user_func([$this, $method]);
                break;

            case 'extract':
                $this->extract();
                break;

            case 'validate':
                $plugin = post('namespace');
                $pluginManager = PluginManager::instance();
                $required = $this->findDependences($plugin);
                if (count($required)) {
                    foreach ($required as $require) {
                        $disable = $require[1];
                        if ($disable === true) {
                            $pluginManager->deletePlugin($plugin);
                            throw new \SystemException(
                                trans(
                                    'planetadeleste.privateplugins::lang.repo.missing_dependence',
                                    ['name' => $plugin, 'dependence' => $require[0]]
                                )
                            );
                        }
                    }
                }
                break;

            case 'composer':
                list($author, $plugin) = explode('.', strtolower(post('namespace')));
                $destination = plugins_path($author.'/'.$plugin);
                $composer = ComposerManager::instance();
                $composer->run($destination, 'install --no-interaction');
                break;

            case 'complete':
                $manager = UpdateManager::instance();
                $manager->update();
                Flash::success(Lang::get('system::lang.install.install_success'));
                return \Backend::redirect('planetadeleste/privateplugins/repos');
                break;

        }


        return ['#formVars' => $this->getFormVars()];
    }


}
