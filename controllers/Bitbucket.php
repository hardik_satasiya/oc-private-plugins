<?php
/**
 * Alvis ERP
 * Created by alvaro.
 * User: alvaro
 * Date: 19/12/17
 * Time: 08:22 AM
 */

namespace PlanetaDelEste\PrivatePlugins\Controllers;


use Illuminate\Routing\Controller;

class Bitbucket extends Controller
{
    public function auth()
    {
        $provider = new \Stevenmaguire\OAuth2\Client\Provider\Bitbucket([
            'clientId'          => config('bitbucket.connections.oauth2.consumer_key'),
            'clientSecret'      => config('bitbucket.connections.oauth2.consumer_secret'),
            'redirectUri'       => url('/planetadeleste_privateplugins_login/bitbucket')
        ]);

        if($token = session('bitbucket_access_token')) {
            $bitbucket = new \Bitbucket\API\Repositories();
            $bitbucket->getClient()->addListener(
                new \Bitbucket\API\Http\Listener\OAuth2Listener(
                    array('access_token'  => $token)
                )
            );

            return \Backend::redirect('planetadeleste/privateplugins/repos/create');
        }


        if(!get('code')) {
            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl();
            session(['bitbucket_oauth2state' => $provider->getState()]);

            return redirect($authUrl);
        } else if(!get('state') || get('state') !== session('bitbucket_oauth2state')) {
            app('session')->forget('bitbucket_oauth2state');
            return 'Invalid Code';
        } else {

            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);

            session(['bitbucket_access_token' => $token->getToken()]);

            return $this->auth();
        }
    }
}