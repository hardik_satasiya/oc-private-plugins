<?php namespace PlanetaDelEste\PrivatePlugins\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use System\Classes\SettingsManager;
use \PlanetaDelEste\PrivatePlugins\Models\Settings as SettingsModel;

class Settings extends Controller
{
    public $implement = [
        'Backend\Behaviors\FormController'    ];
    
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'access_settings' 
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PlanetaDelEste.PrivatePlugins', 'privateplugins');
        SettingsManager::setContext('PlanetaDelEste.PrivatePlugins', 'planetadeleste_privateplugins_settings');
    }

    public function index()
    {
        // Load the editor system settings
        $settings = SettingsModel::instance();
        $this->vars['Settings'] = $settings->toArray();

        $this->asExtension('FormController')->update($settings->id);
        $this->pageTitle = trans('planetadeleste.privateplugins::lang.plugin.name');
    }

    public function index_onSave()
    {
        $settings = SettingsModel::instance();
        return $this->asExtension('FormController')->update_onSave( $settings->id );
    }
}
