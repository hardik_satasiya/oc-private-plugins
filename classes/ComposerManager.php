<?php
/**
 * Alvis ERP
 * Created by alvaro.
 * User: alvaro
 * Date: 21/12/17
 * Time: 10:24 AM
 */

namespace PlanetaDelEste\PrivatePlugins\Classes;


use File;
use GuzzleHttp\Client;
use function GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\RequestOptions;
use October\Rain\Support\Traits\Singleton;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ComposerManager
{
    use Singleton;

    private $path;
    private $cwd;
    private $local = false;

    /**
     * @throws \Exception
     */
    protected function init()
    {
        if (!$this->check()) {
            $this->download();
        }
    }

    /**
     * @param $path
     *
     * @throws \Exception
     */
    public function install($path)
    {
        $this->run($path, 'install');
    }

    /**
     * @param string   $path
     * @param string   $cmd
     * @param callable $callback function($type, $buffer){ }
     *
     * @throws \Exception
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function run($path, $cmd, callable $callback = null)
    {
        $path = rtrim($path, '/');

        if (!\File::isDirectory($path)) {
            throw new \Exception(trans('planetadeleste.privateplugins::lang.repo.not_is_directory', ['name' => $path]));
        }

        if (!\File::exists($path.'/composer.json')) {
            throw new \Exception(
                trans('planetadeleste.privateplugins::lang.repo.file_not_found', ['name' => $path.'/composer.json'])
            );
        }

        $composerPath = ($this->local) ? sprintf('php %s', $this->path) : $this->path;

        $this->prepareComposerJson($path);

        $install = new Process(sprintf('cd %s && %s %s', $path, $composerPath, $cmd));
        $install->setEnv(['COMPOSER_HOME' => $this->cwd]);
        $install->setTimeout(3600);
        $install->start();
        $install->wait($callback);
    }

    public function check()
    {
        $process = new Process('whereis composer');
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        } else {
            $composerPaths = explode(" ", $process->getOutput());
            if (count($composerPaths) > 1) {
                array_shift($composerPaths);
                $composerPath = trim($composerPaths[0]);

                // Set composer path
                $this->path = $composerPath;

                $process = new Process($composerPath.' -V');
                $process->setEnv(['COMPOSER_HOME' => $process->getWorkingDirectory()]);
                $process->run();

                if ($process->isSuccessful()) {
                    // Set working directory
                    $this->cwd = $process->getWorkingDirectory();

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function download()
    {
        $file = storage_path('/app/composer.phar');
        $resource = fopen($file, 'w+');

        if ($resource === false) {
            throw new \Exception('Could not open: '.$file);
        }
        $stream = stream_for($resource);

        $options = [
            RequestOptions::SINK            => $stream, // the body of a response
            RequestOptions::CONNECT_TIMEOUT => 10.0,    // request
            RequestOptions::TIMEOUT         => 60.0,    // response
            RequestOptions::HEADERS         => [
                "Cache-Control: no-cache",
            ]
        ];

        $client = new Client(['base_uri' => 'https://getcomposer.org/']);
        $response = $client->request('GET', 'installer', $options);
        $stream->close();

        if ($response->getStatusCode() === 200) {
            $this->path = $file;
            $this->local = true;
        };
    }

    /**
     * @param $pluginPath
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function prepareComposerJson($pluginPath)
    {
        $pluginPath = rtrim($pluginPath, '/');

        if (!File::exists($pluginPath.'/composer.json')) {
            return;
        }

        $pluginComposer = json_decode(File::get($pluginPath.'/composer.json'), true);
        $pluginComposer['replace'] = array_merge(array_get($pluginComposer, 'replace', []), $this->ocDependencies());
        File::put($pluginPath.'/composer.json', json_encode($pluginComposer));
    }

    protected function ocDependencies()
    {
        return [
            "illuminate/auth"          => "5.5.*",
            "illuminate/broadcasting"  => "5.5.*",
            "illuminate/bus"           => "5.5.*",
            "illuminate/cache"         => "5.5.*",
            "illuminate/config"        => "5.5.*",
            "illuminate/console"       => "5.5.*",
            "illuminate/container"     => "5.5.*",
            "illuminate/contracts"     => "5.5.*",
            "illuminate/cookie"        => "5.5.*",
            "illuminate/database"      => "5.5.*",
            "illuminate/encryption"    => "5.5.*",
            "illuminate/events"        => "5.5.*",
            "illuminate/filesystem"    => "5.5.*",
            "illuminate/hashing"       => "5.5.*",
            "illuminate/http"          => "5.5.*",
            "illuminate/log"           => "5.5.*",
            "illuminate/mail"          => "5.5.*",
            "illuminate/notifications" => "5.5.*",
            "illuminate/pagination"    => "5.5.*",
            "illuminate/pipeline"      => "5.5.*",
            "illuminate/queue"         => "5.5.*",
            "illuminate/redis"         => "5.5.*",
            "illuminate/routing"       => "5.5.*",
            "illuminate/session"       => "5.5.*",
            "illuminate/support"       => "5.5.*",
            "illuminate/translation"   => "5.5.*",
            "illuminate/validation"    => "5.5.*",
            "illuminate/view"          => "5.5.*",
            "tightenco/collect"        => "5.5.*"
        ];
    }
}