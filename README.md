# OctoberCMS PrivatePlugins plugin 
Manage private plugins, from Github or Bitbucket repositories. Plugins that are not in OctoberCMS Marketplace.

![OC Badge](https://img.shields.io/badge/OctoberCMS%20Plugin-+1.0.430-orange.svg?style=flat-square)
[![Total Downloads](https://poser.pugx.org/planetadeleste/privateplugins/downloads?format=flat-square)](https://packagist.org/packages/planetadeleste/privateplugins)
[![Latest Stable Version](https://poser.pugx.org/planetadeleste/privateplugins/v/stable?format=flat-square)](https://packagist.org/packages/planetadeleste/privateplugins)
[![Latest Unstable Version](https://poser.pugx.org/planetadeleste/privateplugins/v/unstable?format=flat-square)](https://packagist.org/packages/planetadeleste/privateplugins)
[![License](https://poser.pugx.org/planetadeleste/privateplugins/license?format=flat-square)](https://packagist.org/packages/planetadeleste/privateplugins)

### Install
Download or clone the repo. Uncompress or copy the content inside `/plugins/planetadeleste/privateplugins` folder. The file `Plugin.php` must be in root of that folder.

After that, run `composer install` inside plugin folder (where `composer.json` file is located)

### Features
- Install / Update private plugins, directly from repository.
- Detect plugins in official marketplace.
- Detect if repository is a valid OC plugin. Only plugins can be installed.
- Dependencies detection.
- Detect if plugin is installed
- Automatic deploy and run Composer if `composer.json` is detected.
- **Soon!** - Inform about new updates.
- **Soon!** - Automatic install new updates. Configured from each installed plugin.

### Settings

> Use Token method for Github and OAuth2 for Bitbucket 

![Imgur](https://i.imgur.com/epBQgOt.png)

### List repositories

![Imgur](https://i.imgur.com/LSJaW9n.png)

### Marketplace Detection
![Marketplace detection](https://i.imgur.com/T1TDgjY.png)  
Before install any plugin, detect if is published in official OctoberCMS Marketplace. In that case, is recommended to be installed from the Updates panel

### Valid OC plugin detection
![Valid OC plugin detection](https://i.imgur.com/58deerx.png)  

### Composer detection
![Composer detection](https://i.imgur.com/4Da6MWd.png)

### Dependencies detection
![Dependencies detection](https://i.imgur.com/k6fMuCr.png)

### Detect plugin installed
![Detect plugin installed](https://i.imgur.com/VApKANA.png)