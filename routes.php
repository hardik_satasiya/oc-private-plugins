<?php

Route::group(
    [
        'middleware' => ['web'],
        'prefix'     => 'planetadeleste_privateplugins_login'
    ],
    function () {
        Route::get('bitbucket', '\PlanetaDelEste\PrivatePlugins\Controllers\Bitbucket@auth');
    }
);