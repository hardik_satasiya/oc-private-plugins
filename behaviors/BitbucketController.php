<?php

namespace PlanetaDelEste\PrivatePlugins\Behaviors;


use Backend\Classes\ControllerBehavior;
use Flash;
use GrahamCampbell\Bitbucket\Facades\Bitbucket;
use function GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Client;
use Guzzle\Http\Client as GuzzleClient;
use System\Classes\PluginManager;
use System\Classes\UpdateManager;

/**
 * Class BitbucketController
 * @package PlanetaDelEste\PrivatePlugins\Behaviors
 *
 * @property \PlanetaDelEste\PrivatePlugins\Controllers\Repos $controller
 */
class BitbucketController extends ControllerBehavior
{

    protected $source = 'bitbucket';

    /**
     * BitbucketController constructor.
     *
     * @param $controller
     *
     * @throws \ApplicationException
     */
    public function __construct($controller)
    {
        parent::__construct($controller);

        $this->addViewPath('$/planetadeleste/privateplugins/behaviors/bitbucket');
    }

    /**
     * @param $baseUri
     * @param $file
     * @param $filename
     *
     * @throws \Exception
     */
    protected function curlDownload($baseUri, $file, $filename)
    {
        $resource = fopen($file, 'w+');

        if ($resource === false) {
            throw new \Exception('Could not open: '.$file);
        }

        // set download url of repository for the relating node
        $ch = curl_init();

        curl_setopt_array(
            $ch,
            [
                CURLOPT_URL            => $baseUri.'get/'.$filename,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 60,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "GET",
                CURLOPT_FILE           => $resource,
                CURLOPT_HTTPHEADER     => [
                    "Authorization: Bearer ".session('bitbucket_access_token'),
                    "Cache-Control: no-cache",
                ],
            ]
        );

        // run the curl command
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            throw new \Exception(curl_error($ch));
        }

        // close curl and file functions
        curl_close($ch);
        fclose($resource);


        debug($result);
        debug($file);
        debug($baseUri);
    }

    /**
     * @param $baseUri
     * @param $file
     * @param $filename
     *
     * @return bool
     * @throws \Exception
     */
    protected function guzzleDownload($baseUri, $file, $filename)
    {
        $resource = fopen($file, 'w+');

        if ($resource === false) {
            throw new \Exception('Could not open: '.$file);
        }
        $stream = stream_for($resource);

        $options = [
            RequestOptions::SINK            => $stream, // the body of a response
            RequestOptions::CONNECT_TIMEOUT => 10.0,    // request
            RequestOptions::TIMEOUT         => 60.0,    // response
            RequestOptions::HEADERS         => [
                'Authorization' => 'Bearer '.session('bitbucket_access_token'),
                "Cache-Control: no-cache",
            ]
        ];

        $client = new Client(['base_uri' => $baseUri]);
        $response = $client->request('GET', 'get/'.$filename, $options);
        $stream->close();
//        fclose($resource);

        return $response->getStatusCode() === 200;

    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function downloadBitbucketRepo()
    {
        $baseUri = "https://bitbucket.org/{$this->controller->account}/{$this->controller->slug}/";


        $filename = post('node', 'master').'.zip';
        $file = temp_path($filename);
        if (\File::exists($file)) {
            \File::delete($file);
        }

        $this->setAccessToken();

        $this->guzzleDownload($baseUri, $file, $filename);

        $this->controller->vars['file'] = $file;


//        return ['^#executePopup' => \Form::hidden('file', $file)];
    }

    public function setAccessToken()
    {
        $baseUri = 'https://bitbucket.org/site/oauth2/';
        $client = new Client(['base_uri' => $baseUri]);
        $options = [
            RequestOptions::AUTH        => [
                config('bitbucket.connections.oauth2.consumer_key'),
                config('bitbucket.connections.oauth2.consumer_secret')
            ],
            RequestOptions::FORM_PARAMS => [
                'grant_type' => 'client_credentials'
            ],
            RequestOptions::HEADERS     => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ];

        $response = $client->request('POST', 'access_token', $options);
        $response = json_decode($response->getBody()->getContents(), true);
        $accessToken = array_get($response, 'access_token');

        session(['bitbucket_access_token' => $accessToken]);

        return $accessToken;

    }

    public function getRepoAvatar($url)
    {
        if(!session('bitbucket_access_token')) {
            $this->setAccessToken();
        }

        $client = new GuzzleClient();
        $options = [
            RequestOptions::HEADERS         => [
                'Authorization' => 'Bearer '.session('bitbucket_access_token'),
                "Cache-Control: no-cache",
            ]
        ];
        $response = $client->get($url, $options)->send();
//        $response = $client->send($request);

        if($response->isSuccessful()) {
            debug($response->getBody());
        }
    }

    public function getBitbucketRepos()
    {

        try {

            /*if (!session('bitbucket_access_token')) {
                $this->controller->vars['hideTableHead'] = true;
                return $this->makePartial('login_php');
            }*/

            /** @var \Buzz\Message\Response $api */
            $api = Bitbucket::api('Repositories')->all(
                config('bitbucket.account'),
                ['sort' => '-updated_on', 'page' => post('page', 1)]
            );
            $repos = json_decode($api->getContent());
            $pages = 1;

            if ($repos->size > $repos->pagelen) {
                $pages = ceil($repos->size / $repos->pagelen);
            }

            $this->controller->vars['pages'] = $pages;
            $this->controller->vars['page'] = $repos->page;
            $this->controller->vars['pagelen'] = $repos->pagelen;
            $this->controller->vars['total'] = $repos->size;
            $this->controller->vars['pagesFrom'] = ($repos->pagelen * ($repos->page - 1)) + 1;
            $pagesTo = $repos->pagelen * $repos->page;
            $this->controller->vars['pagesTo'] = ($pagesTo > $repos->size) ? $repos->size : $pagesTo;
            $this->controller->vars['repos'] = $repos;
            return $this->makePartial('list_php');

        } catch (\SystemException $e) {
            trace_log($e);
            Flash::error($e->getMessage());
        }
    }

    /**
     * @return array
     */
    public function getBitbucketRepo()
    {
        /** @var \Buzz\Message\Response $repo */
        /** @var \Bitbucket\API\Repositories\Repository $api */
        $api = Bitbucket::api('Repositories\Repository');
        $repo = $api->get($this->controller->account, $this->controller->slug);
        return json_decode($repo->getContent(), true);
    }

    /**
     * @return mixed
     */
    public function getBitbucketRepoCommits()
    {
        /** @var \Buzz\Message\Response $commits */
        /** @var \Bitbucket\API\Repositories\Commits $api */
        $api = Bitbucket::api('Repositories\Commits');
        $commits = $api->all($this->controller->account, $this->controller->slug);
        return json_decode($commits->getContent(), true);
    }

    public function loadBitbucketRepo()
    {
        $content = null;

        if ($this->controller->slug && $this->controller->account) {
//            debug($this->getBitbucketRepoCommits());

            /** @var \Buzz\Message\Response $repo */
            /** @var \Bitbucket\API\Repositories\Repository $api */
            $api = Bitbucket::api('Repositories\Repository');
            $repo = $api->branches($this->controller->account, $this->controller->slug);
            $branches = json_decode($repo->getContent(), true);

            $node = array_get($branches, 'master.node');
            $content = $this->prepareBitbucketRepo($node);
            
            // TODO: Make user interaction for repos with more than one branch
            /*if (count($branches) == 1) {

            }*/
        }

        return $content;
    }

    public function prepareBitbucketRepo($node)
    {
        try {
            $api = Bitbucket::api('Repositories\Src');
            $src = $api->get($this->controller->account, $this->controller->slug, $node, '/');
            $response = json_decode($src->getContent(), true);

            $files = collect(array_get($response, 'files', []));
            $pluginFile = $files->where('path', 'Plugin.php');
            $pluginComposerFile = $files->where('path', 'composer.json');

            $this->controller->vars['has_composer'] = ($pluginComposerFile->count() >= 1);

            if ($pluginFile->count()) {
                $this->controller->vars['node'] = $node;

                $rawPlugin = $api->raw($this->controller->account, $this->controller->slug, $node, '/Plugin.php');
                $rawResponse = preg_split("/\r\n|\n|\r/", $rawPlugin->getContent());

                // Find namespace
                $namespace = $this->controller->getRawNamespace($rawResponse);
                $this->controller->vars['namespace'] = str_replace('\\', '.', $namespace);

                // Find dependencies
                $this->controller->vars['dependences'] = $pluginDependences = $this->controller->getRawDependencies($rawResponse);

                $manager = UpdateManager::instance();
                $search = $manager->requestServerData('plugin/search', ['query' => str_replace('\\', ' ', $namespace)]);

                if (count($search)) {
                    $marketplace = $this->controller->makePartial(
                        'marketplace_plugin_info_php',
                        ['plugin' => $search[0]]
                    );
                    $this->controller->vars['force'] = true;
                    $this->controller->vars['install'] = true;
                } else {
                    /** @var PluginManager $pluginsManager */
                    $pluginsManager = PluginManager::instance();
                    $pluginRegistered = $pluginsManager->findByNamespace($namespace);
                    if (!$pluginRegistered) {
                        $this->controller->vars['install'] = true;
                    } else {
                        $this->controller->vars['update'] = true;
                    }

                    $repo = $this->getBitbucketRepo();
                    $marketplace = $this->controller->makePartial(
                        'plugin_info_php',
                        [
                            'plugin' => [
                                'name'        => array_get($repo, 'name'),
                                'image'       => array_get($repo, 'links.avatar.href'),
                                'author'      => array_get($repo, 'owner.display_name'),
                                'description' => array_get($repo, 'description'),
                                'is_private'  => array_get($repo, 'is_private'),
                            ]
                        ]
                    );
                }

                // Check dependences
                if(count($pluginDependences)) {
                    foreach ($pluginDependences as $require) {
                        $disable = $require[1];
                        if ($disable === true) {
                            $this->controller->vars['install'] = false;
                        }
                    }
                }

                $content = $this->controller->makePartial('repo_modal_message_php', ['content' => $marketplace]);

            } else {
                $hint = $this->controller->makeHintPartial(
                    'hint_warn_php',
                    [
                        'type'    => 'warning',
                        'icon'    => 'icon-'.$this->source,
                        'repo'    => studly_case($this->source),
                        'message' => trans('planetadeleste.privateplugins::lang.repo.plugin_not_found')
                    ]
                );

                $content = $this->controller->makePartial('repo_modal_message_php', ['content' => $hint]);
            }

            return $content;
        } catch (\SystemException $e) {
            trace_log($e);
            Flash::error($e->getMessage());
        } catch (\ApplicationException $e) {
            trace_log($e);
            Flash::error($e->getMessage());
        }
    }

}