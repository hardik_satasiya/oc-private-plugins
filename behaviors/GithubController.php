<?php

namespace PlanetaDelEste\PrivatePlugins\Behaviors;


use Backend\Classes\ControllerBehavior;
use Github\Client;
use Github\ResultPager;
use GrahamCampbell\GitHub\Facades\GitHub;
use function GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Client as GuzzleClient;
use System\Classes\PluginManager;
use System\Classes\UpdateManager;

/**
 * Class GithubController
 * @package PlanetaDelEste\PrivatePlugins\Behaviors
 *
 * @property \PlanetaDelEste\PrivatePlugins\Controllers\Repos $controller
 */
class GithubController extends ControllerBehavior
{
    protected $source = 'github';

    /**
     * GithubController constructor.
     *
     * @param \Backend\Classes\Controller $controller
     *
     * @throws \ApplicationException
     */
    public function __construct($controller)
    {
        parent::__construct($controller);

        $this->addViewPath('$/planetadeleste/privateplugins/behaviors/github');
    }

    /**
     * @return Client
     */
    private function client()
    {
        return GitHub::connection();
    }

    /**
     * @return mixed
     * @throws \SystemException
     */
    public function getGithubRepos()
    {
        $client = $this->client();
        $api = $client->me();
//        $api->setPerPage(10);

        $paginator  = new ResultPager($client);
        $result     = $paginator->fetch($api, 'repositories', ['owner', 'updated_at', 'desc']);

        $this->controller->vars['pages'] = null;
        $this->controller->vars['repos'] = $result;

        return $this->makePartial('list_php');
    }

    /**
     * @return array|null
     */
    public function getGithubRepo()
    {
        if($this->controller->slug) {
            return $this->client()->repo()->show($this->controller->account, $this->controller->slug);
        }

        return null;
    }

    /**
     * @return mixed|null
     * @throws \ApplicationException
     * @throws \SystemException
     */
    public function loadGithubRepo()
    {
        $content = null;
        if($this->controller->slug) {
            $repo = $this->getGithubRepo();
            $this->controller->vars['node'] = $repo['default_branch'];

            $content = $this->prepareGithubRepo($repo);
        }

        return $content;
    }

    /**
     * @param $repo
     *
     * @return mixed
     * @throws \ApplicationException
     * @throws \SystemException
     */
    public function prepareGithubRepo($repo)
    {
        list($username, $slug) = [$this->controller->account, $this->controller->slug];
        $contents = $this->client()->repo()->contents();
        $files = collect($contents->show($username, $slug));

        $pluginFile = $files->where('path', 'Plugin.php');
        $pluginComposerFile = $files->where('path', 'composer.json');

        $this->controller->vars['has_composer'] = ($pluginComposerFile->count() >= 1);

        if ($pluginFile->count()) {

            $rawPlugin = $contents->show($username, $slug, '/Plugin.php');
            $rawResponse = preg_split("/\r\n|\n|\r/", base64_decode($rawPlugin['content']));

            // Find namespace
            $namespace = $this->controller->getRawNamespace($rawResponse);
            $this->controller->vars['namespace'] = str_replace('\\', '.', $namespace);

            // Find dependencies
            $this->controller->vars['dependences'] = $pluginDependences = $this->controller->getRawDependencies($rawResponse);

            $manager = UpdateManager::instance();
            $search = $manager->requestServerData('plugin/search', ['query' => str_replace('\\', ' ', $namespace)]);

            if (count($search)) {
                $marketplace = $this->controller->makePartial(
                    'marketplace_plugin_info_php',
                    ['plugin' => $search[0]]
                );
                $this->controller->vars['force'] = true;
                $this->controller->vars['install'] = true;
            } else {
                /** @var PluginManager $pluginsManager */
                $pluginsManager = PluginManager::instance();
                $pluginRegistered = $pluginsManager->findByNamespace($namespace);
                if (!$pluginRegistered) {
                    $this->controller->vars['install'] = true;
                } else {
                    $this->controller->vars['update'] = true;
                }

                $marketplace = $this->controller->makePartial(
                    'plugin_info_php',
                    [
                        'plugin' => [
                            'name'        => array_get($repo, 'name'),
                            'image'       => null,
                            'author'      => array_get($repo, 'owner.login'),
                            'description' => array_get($repo, 'description'),
                            'is_private'  => array_get($repo, 'private'),
                        ]
                    ]
                );
            }

            // Check dependences
            if(count($pluginDependences)) {
                foreach ($pluginDependences as $require) {
                    $disable = $require[1];
                    if ($disable === true) {
                        $this->controller->vars['install'] = false;
                    }
                }
            }

            $content = $this->controller->makePartial('repo_modal_message_php', ['content' => $marketplace]);

        } else {
            $hint = $this->controller->makeHintPartial(
                'hint_warn_php',
                [
                    'type'    => 'warning',
                    'icon'    => 'icon-'.$this->source,
                    'repo'    => studly_case($this->source),
                    'message' => trans('planetadeleste.privateplugins::lang.repo.plugin_not_found')
                ]
            );

            $content = $this->controller->makePartial('repo_modal_message_php', ['content' => $hint]);
        }

        return $content;

    }

    /**
     * @throws \Exception
     */
    public function downloadGithubRepo()
    {
        $archive = $this->client()->repo()->contents()->archive($this->controller->account, $this->controller->slug, 'zipball');
        if(is_string($archive)) {

            $filename = post('node', 'master').'.zip';
            $file = temp_path($filename);
            if (\File::exists($file)) {
                \File::delete($file);
            }

            $resource = fopen($file, 'w+');
            fputs($resource, $archive);
            fclose($resource);

            $this->controller->vars['file'] = $file;
        } else {
            trace_log($archive);
        }
    }

    /**
     * @param $baseUri
     * @param $file
     * @param $filename
     *
     * @return bool
     * @throws \Exception
     */
    protected function guzzleDownload($baseUri, $file, $filename)
    {
        $resource = fopen($file, 'w+');

        if ($resource === false) {
            throw new \Exception('Could not open: '.$file);
        }
        $stream = stream_for($resource);

        $options = [
            RequestOptions::SINK            => $stream, // the body of a response
            RequestOptions::CONNECT_TIMEOUT => 10.0,    // request
            RequestOptions::TIMEOUT         => 60.0,    // response
            RequestOptions::HEADERS         => [
                'Authorization' => 'token '.config('github.connections.main.token'),
                "Cache-Control: no-cache",
            ]
        ];

        $client = new GuzzleClient(['base_uri' => $baseUri]);
        $response = $client->request('GET', 'get/'.$filename, $options);
        $stream->close();
//        fclose($resource);

        return $response->getStatusCode() === 200;

    }
}